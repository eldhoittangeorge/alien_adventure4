//
//  SortLeastToGreatest.swift
//  Alien Adventure
//
//  Created by Jarrod Parkes on 10/4/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

extension Hero {
    
    func sortLeastToGreatest(inventory: [UDItem]) -> [UDItem] {
        
        
        
        
        
        var sortedItems = inventory
        sortedItems.sortInPlace(){ (first:UDItem, second:UDItem) -> Bool in
            if (first.rarity.hashValue < second.rarity.hashValue){
                return true
            } else if (first.rarity.hashValue == second.rarity.hashValue){
                return (first.baseValue < second.baseValue)
            }
            return false
        }
        
        return sortedItems
        
        
        
    }
    
}

// If you have completed this function and it is working correctly, feel free to skip this part of the adventure by opening the "Under the Hood" folder, and making the following change in Settings.swift: "static var RequestsToSkip = 5"