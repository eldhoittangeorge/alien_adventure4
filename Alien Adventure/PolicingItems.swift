//
//  PolicingItems.swift
//  Alien Adventure
//
//  Created by Jarrod Parkes on 10/4/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

extension Hero {
    
    func policingItems(inventory: [UDItem], policingFilter: UDItem throws -> Void) -> [UDPolicingError:Int] {
        //        return [UDPolicingError:Int]()
        
        var errorDictionary:
            [UDPolicingError:Int] = [.ItemFromCunia:0, .NameContainsLaser:0, .ValueLessThan10: 0]
        
        for item in inventory {
            do {
                try policingFilter(item)
            } catch UDPolicingError.ItemFromCunia {
                errorDictionary[.ItemFromCunia]! += 1
            } catch UDPolicingError.NameContainsLaser{
                errorDictionary[.NameContainsLaser]! += 1
            } catch UDPolicingError.ValueLessThan10{
                errorDictionary[.ValueLessThan10]! += 1
            } catch {
                //I don't know what the default case should be... help?
            }
            
        }
        
        return errorDictionary
        
        
    }
}

// If you have completed this function and it is working correctly, feel free to skip this part of the adventure by opening the "Under the Hood" folder, and making the following change in Settings.swift: "static var RequestsToSkip = 1"