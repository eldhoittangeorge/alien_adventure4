//
//  RemoveDuplicates.swift
//  Alien Adventure
//
//  Created by Jarrod Parkes on 10/4/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

extension Hero {
    
    
    
    
    // VERY CLEVER use of Set() as mentioned on this forum post:
    // https://discussions.udacity.com/t/remove-duplicates-removeduplicates-swift/45328
    
    
    // since the instructions did not specify a particular type of "sort" to be returned all
    // we have to do is get the unique set, which is what Set() does for us :)
    
    func removeDuplicates(inventory: [UDItem]) -> [UDItem] {
        let noDupes = Array(Set(inventory))
        return noDupes
        
        
    }
    
    
}
